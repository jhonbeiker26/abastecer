$(document).ready(function(){
    formSubmit();
});

function formSubmit(){
    $('#login-form').submit(function(e){ // Selecciona el formulario e indica el evento submit
        e.preventDefault(); // Desactiva el la accion por defecto del evento.
        var username = $('#username').val(); // Se obtiene el valor de 'username'.
        var password = $('#password').val(); // Se obtiene el valor de 'password'.
        $.ajax({ // OBjeto AJAX
            type: "POST", // Método de envio de datos.
            url: "index.php?controller=Login&action=IniciarSesion", // Ruto donde se enviaran los datos.
            data: { username: username, password: password }, // Objeto JSON con los datos
            beforeSend: function () { // Antes de enviar los datos
                $('#response-text').html('<div class="progress green lighten-3"><div class="indeterminate green"></div></div>');
            },
            success: function(response){ // Si el envio de datos fue exitoso.
                if(response >= '1'){ // Si la respuesta es mayor o igual a uno.
                    document.location = "index.php?controller=Login&action=Home&id=" + response; // Redirecciona a otro controlador.
                } 
                else{
                    $('#response-text').html('<p class=""><i class="icon-warning"></i>Credenciales Incorrectas.</p>');
                    // $('#username').val() = "";
                    // $('#password').val() = "";
                    /* document.getElementById('username').value = "";
                    var password = document.getElementById('password').value = ""; */
                }
            },
            error: function (xhr, status) {
                $('#response-text').html('<p class=""><i class="icon-sentiment_dissatisfied"></i>Disculpe, Ocurrio un error inesperado.</p>');
            }
        });
    });
}