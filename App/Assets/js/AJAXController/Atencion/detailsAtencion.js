$(document).ready(function () {
    //obtener datos de la url
    function getVariableGetByName() {
        var variables = {};
        var arreglos = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            variables[key] = value;
        });
        return variables;
    }
    var idAtencion = getVariableGetByName()["idAtencion"];
    var clap;
    var fecha;

    CargaAsincrona();

    $("#Agregar").click(function(){
        swal({
            text:"Agrega Beneficiarios",
            content:"input",
            button:{
                text:"Buscar",
                closeModal:false,
            }
        })
        .then(function(value){
            if(value !== null){
                RegisterBeneficiarios(value,idAtencion,clap,fecha);
                CargaAsincrona();
            }
        });
    });

    function RegisterBeneficiarios(cedula,idAtencion,clap,fecha){
        $.ajax({
            type:'POST',
            dataType:'json',
            data:{
                cedula: cedula,
                idFecha: idAtencion,
                clap: clap,
                date: fecha
            },
            url:"index.php?controller=Atencion&action=registerDataPerson",
            beforeSend: function(){
                var textPreloader='<div class="progress no-margin">\n' +
                    ' <div class="determinate" style="width: 0%"></div>\n' +
                    ' </div>';
                var preloader=$("#response-preloader").html(textPreloader);
            },
            success: function(data){
                if(data === null){
                    swal( "¡Oh no! " , "Cedula "+cedula+" no esta registrado en el sistema." , "error" );
                }
                else if(data === 1){
                    swal( "¡Oh no! " , "Esta familia ya fue atendida este mes." , "error" );
                }
                else{
                    if(data.apellidoFamilia !== undefined){
                        swal("Familia "+data.apellidoFamilia+ " ha sido atendio con éxito.", {
                            icon: "success",
                            buttons: false,
                            timer: 3000,
                        });
                        $('#close').removeAttr('disabled','disabled');
                    }
                    else{
                        swal( "¡Oh no! " , "La familia ingresada no pertenece a este CLAP. Es del CLAP " + data.nombreClap+".", "warning" );
                    }
                }
                $('.determinate').css('width','100%');
            },
            error: function(){
                swal("¡Oh no!" , "Ocurrio un error inesperado,refresque la pagina e intentelo de nuevo" , "error" );
                $('.determinate').css('width','100%');
            }
        });
    }

    function CargaAsincrona(){
        $.ajax({
            type:'GET',
            dataType:'json',
            data:{
                idAtencion: idAtencion
            },
            url: "index.php?controller=Atencion&action=detailsData",
            beforeSend: function(){
                var textPreloader='<div class="progress no-margin">\n' +
                    ' <div class="determinate" style="width: 0%"></div>\n' +
                    ' </div>';
                var preloader=$("#response-preloader").html(textPreloader);
            },
            success: function(data){
                $("#Parroquia").html("<b>Parroquia:</b> " + data.parroquia);
                $("#nombreClap").html("<b>Nombre del CLAP:</b> " + data.nombreClap);
                $("#FamiliaBenefiaciadas").html("<b>Familias Beneficiadas:</b> "+data.cantidad);
                $("#fechaLimite").html("<b>Fecha Limite:</b> " + data.fechaLimite)
                clap = data.idClap;
                fecha = data.fechaAtencion;

                $("#date").val(data.fechaAtencion);
                $("#tipoAtencion option[value='"+ data.tipoAtencion +"']").prop("selected",true);
                $('#observacion').val(data.observacion);
                M.textareaAutoResize($('#observacion'));
                M.updateTextFields();
                $('.determinate').css('width','100%');
            },
            error:function () {
                $('.determinate').css('width','100%');
            }
        });
    }
});