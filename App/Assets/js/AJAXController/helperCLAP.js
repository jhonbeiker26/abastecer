$(document).ready(function(){
    $("select[name=parroquia]").change(function(){
        var parroquia=$('select[name=parroquia]').val();

        $.ajax({
            type: 'POST',
            dataType: 'json',
            data:{parroquia:parroquia},
            url:"index.php?controller=helperCLAP&action=getCLAP",

            beforeSend:function () {
                $("#clap").html("<option disabled>Cargando...</option>");
            },
            success:function (data) {
                var acum="<option disabled selected>Elige un CLAP</option>";
                //compruebo si viene vacio el array de objetos


                if(data[0] === null){
                    acum="<option disabled selected>No hay CLAP</option>";
                }else{//si no viene vacio lo recorro
                    data.forEach(function(clap,index){
                        acum += '<option value=' + clap.idClap + '>' + clap.nombreClap+ '</option>';
                    });
                }

                //finalmente suplanto el contenido del select
                $("#clap").html(acum);
                $('select').formSelect();
            }
        });
    });
});