<?php
    class LoginController extends BaseController{
        public function __construct(){
            parent::__construct();
        }
        public function index(){
            $this->view("Login/Login");
        }

        public function Home(){
            $login=new Login();
            $login->getById($_GET['id']);
            $this->view('Home/Home');
        }

        public function IniciarSesion(){
            $user = new Login();
            $user->setUsername($_POST["username"]);
            $user->setPassword($_POST["password"]);
            $band = $user->Acceder();
            echo $band;
        }

        public function cerrarSesion(){
            session_destroy();
            $this->redirect();
        }
    }
?>