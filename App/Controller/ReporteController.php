<?php
    use Spipu\Html2Pdf\Html2Pdf;
    class ReporteController extends BaseController{
        public function __construct(){ // Constructor de la clase
            parent::__construct(); // Ejecuta el constructor del padre.
            require_once 'Libs/Vendor/autoload.php';            
        }

        public function index(){
            $reporte = new Reporte();
            $allClaps = $reporte->getClaps();
            $allDenuncias = $reporte->getDenuncias();
            // $allEmpresas = $reporte->getDistribuidoras();
            $this->viewArray('Reporte/GenerarReporte',array(
                'allClaps' => $allClaps,
                // 'allEmpresas' => $allEmpresas
                'allDenuncias' => $allDenuncias
            ));
        }

        public function readCLAP(){

        }

        public function readSolicitud(){
            
        }

        public function readDenuncia(){

        }

        public function readAtencion(){

        }

        public function getClapReport(){
            $clap = new CLAP();
            $id = (int)$_GET['idClap'];
            
        }

        public function getPDF(){
            ob_start(); // Recoge todo el contenido de un include.
            require_once 'View/Reporte/ReportePdf.php'; 
            $html = ob_get_clean(); // Guarda el contenido del archivo en una variable.
            $reporte = new Html2Pdf(); // Instancia el objeto html2pdf
            $reporte->writeHTML($html); // Escribe la vista en el pdf.
            $reporte->output('reporte.pdf'); // Imprime el pdf.
        }
    }
?>