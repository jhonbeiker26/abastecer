<?php
    class SolicitudController extends BaseController{
        public function __construct(){ // Constructor de la clase
            parent::__construct(); // Ejecuta el constructor del padre.
        }

        public function index(){
            $this->view('Solicitud/Solicitud');
        }

        public function register(){
            $this->view('Solicitud/RegistrarSolicitud');
        }

        public function insertData(){

        }

        public function readData(){
            $this->view('Solicitud/ConsultarSolicitud');
        }

        public function updateData(){

        }

        public function deleteData(){

        }

        public function details(){
            
        }
    }
?>