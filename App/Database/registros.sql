-- Registros CLAP
INSERT INTO `clap`
    (codigoClap,codigoSala,nombreClap,rifClap,parroquia,emailClap,tlfClap,nombreComunidad,
    limiteNorteComunidad,limiteSurComunidad,limiteOesteComunidad,limiteEsteComunidad,
    nombreConsejoComunal,rifConsejoComunal,zonaSilencio,cantManzaneros,eje,revisadoEnlace,
    cantViviendas,cantFamilias,estado,idEnlace,idEmpresa) 
    VALUES 
    ("0000-NNNN","ZXCV-1111","Cerro Blanco","J-223344556","Juan de Villegas","vallehondoclap@correo.com",
    "04123456787","La Morita","El Frio","Rey Dormido","Barro Negro","La Menca","Los Sapos",
    "J-998877665",1,50,10,1,100,78,1,1,3);
INSERT INTO `clap`
    (codigoClap,codigoSala,nombreClap,rifClap,parroquia,emailClap,tlfClap,nombreComunidad,
    limiteNorteComunidad,limiteSurComunidad,limiteOesteComunidad,limiteEsteComunidad,
    nombreConsejoComunal,rifConsejoComunal,zonaSilencio,cantManzaneros,eje,revisadoEnlace,
    cantViviendas,cantFamilias,estado,idEnlace,idEmpresa) 
    VALUES 
    ("0000-AAAA","ABCD-1111","Valle Hondo","J-223344556","Juan de Villegas","vallehondoclap@correo.com",
    "04123456787","La Morita","El Frio","Rey Dormido","Barro Negro","La Menca","Los Sapos",
    "J-998877665",1,50,10,1,100,78,1,1,3);
INSERT INTO `clap`
    (codigoClap,codigoSala,nombreClap,rifClap,parroquia,emailClap,tlfClap,nombreComunidad,
    limiteNorteComunidad,limiteSurComunidad,limiteOesteComunidad,limiteEsteComunidad,
    nombreConsejoComunal,rifConsejoComunal,zonaSilencio,cantManzaneros,eje,revisadoEnlace,
    cantViviendas,cantFamilias,estado,idEnlace,idEmpresa) 
    VALUES 
    ("0000-BBBB","ASDF-2222","Cañadita","J-334455667","Catedral","lacanadita@correo.com",
    "04120099887","La Cañada","San Jacinto","El Jeve","San Lorenzo","La Victoria","Los Balurdos",
    "J-334422144",1,24,1,0,245,300,0,3,1);
INSERT INTO `clap`
    (codigoClap,codigoSala,nombreClap,rifClap,parroquia,emailClap,tlfClap,nombreComunidad,
    limiteNorteComunidad,limiteSurComunidad,limiteOesteComunidad,limiteEsteComunidad,
    nombreConsejoComunal,rifConsejoComunal,zonaSilencio,cantManzaneros,eje,revisadoEnlace,
    cantViviendas,cantFamilias,estado,idEnlace,idEmpresa) 
    VALUES 
    ("0000-CCCC","QWER-3333","El Limoncito","J-113322445","Moran","el-limoncito@correo.com",
    "041299887766","El Limocito","La Naranjita","La Perita","La Lechozita","La Mandarina","Campesinitos",
    "J-554433221",0,10,4,1,115,100,1,5,4);
INSERT INTO `clap`
    (codigoClap,codigoSala,nombreClap,rifClap,parroquia,emailClap,tlfClap,nombreComunidad,
    limiteNorteComunidad,limiteSurComunidad,limiteOesteComunidad,limiteEsteComunidad,
    nombreConsejoComunal,rifConsejoComunal,zonaSilencio,cantManzaneros,eje,revisadoEnlace,
    cantViviendas,cantFamilias,estado,idEnlace,idEmpresa) 
    VALUES 
    ("0000-DDDD","TYUI-4444","Santa Rosa","J-55322441","Santa Rosa","larosita12@correo.com",
    "04123214567","La Pastora","La Victoria","El Paraiso","El Carmen","San Pedro","Pastores",
    "J-234564323",1,34,5,0,156,205,1,7,3);

-- Registros Familia

INSERT INTO `grupofamiliar`
    (grupoFamiliar,apellidoFamilia,direccionFamilia,numVivienda,
    numManzana,cantMercadosAsignados,idCLAP) 
    VALUES ("1","Rodriguez","Vereda 15 entre calles 2 y 3","2-5",2,1,1);


-- Registros enlaces politicos
INSERT INTO `enlace_politico`(nombreEnlace,apellidoEnlace,parroquiaEncargado) 
    VALUES ('Juan','Ororzco','Buena Vista');
INSERT INTO `enlace_politico`(idEnlace,nombreEnlace,apellidoEnlace,parroquiaEncargado) 
    VALUES ('Vanesa','Rodríguez','Catedral');
INSERT INTO `enlace_politico`(idEnlace,nombreEnlace,apellidoEnlace,parroquiaEncargado) 
    VALUES ('Mario','Mendoza','Concepción');
INSERT INTO `enlace_politico`(idEnlace,nombreEnlace,apellidoEnlace,parroquiaEncargado) 
    VALUES ('Antonio','Falcón','Felipe Alvarado');
INSERT INTO `enlace_politico`(idEnlace,nombreEnlace,apellidoEnlace,parroquiaEncargado) 
    VALUES ('Ana María','Mendez','Juan de Villegas');
INSERT INTO `enlace_politico`(idEnlace,nombreEnlace,apellidoEnlace,parroquiaEncargado) 
    VALUES ('Laura','Martinez','Juarez');
INSERT INTO `enlace_politico`(idEnlace,nombreEnlace,apellidoEnlace,parroquiaEncargado) 
    VALUES ('Luis','Fuentes','Santa Rosa');
INSERT INTO `enlace_politico`(idEnlace,nombreEnlace,apellidoEnlace,parroquiaEncargado) 
    VALUES ('Vicente','Lozada','Tamaca');
INSERT INTO `enlace_politico`(idEnlace,nombreEnlace,apellidoEnlace,parroquiaEncargado) 
    VALUES ('Daniela','Pérez','Unión');

