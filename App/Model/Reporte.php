<?php 
    class Reporte extends EntidadBase{
        public function __construct(){
            parent::__construct();
        }

        public function getClaps(){
            $sql = "SELECT * FROM clap AS c 
                        INNER JOIN empresa_distribuidora AS e ON c.idEmpresa = e.idEmpresa 
                        ORDER BY idClap ASC"; // Consulta SQL
            $query = $this->DB()->query($sql); // Realiza la consulta SQL.
            if($query){ // Evalua la cansulta
                if($query->rowCount() != 0) { // Si existe al menos un registro...
                    while($row = $query->fetch(PDO::FETCH_OBJ)) { // Recorre un array (tabla) fila por fila.
                        $resultset[] = $row; // Llena el array con cada uno de los registros de la tabla.
                    }
                }
                else{ // Sino...
                    $resultset = null; // Almacena null
                }
            }
            return $resultset; // Finalmente retornla el arreglo con los elementos.
        }

        public function getClapById($id){
            
        }

        public function getDenuncias(){
            $sql = "SELECT * FROM denuncias 
                        INNER JOIN miembrofamilia ON denuncias.idIntegrante = miembrofamilia.idIntegrante 
                        INNER JOIN grupofamiliar ON miembrofamilia.idFamilia = grupofamiliar.idFamilia 
                        INNER JOIN clap ON grupofamiliar.idCLAP = clap.idClap";
            $query = $this->DB()->query($sql); // Realiza la consulta SQL.
            if($query){ // Evalua la cansulta
                if($query->rowCount() != 0) { // Si existe al menos un registro...
                    while($row = $query->fetch(PDO::FETCH_OBJ)) { // Recorre un array (tabla) fila por fila.
                        $resultset[] = $row; // Llena el array con cada uno de los registros de la tabla.
                    }
                }
                else{ // Sino...
                    $resultset = null; // Almacena null
                }
            }
            return $resultset; // Finalmente retornla el arreglo con los elementos.         
        }

        public function getSolicitudes(){

        }

        public function getAtenciones(){

        }

        public function getDistribuidoras(){
            $query = $this->DB()->prepare("SELECT * FROM empresa_distribuidora ORDER BY idEmpresa ASC"); // Consulta SQL.
            $query->execute();
            if($query){ // Evalua la cansulta
                if($query->rowCount() != 0) { // Si existe al menos un registro...
                    while($row = $query->fetch(PDO::FETCH_OBJ)) { // Recorre un array (tabla) fila por fila.
                        $resultset[] = $row; // Llena el array con cada uno de los registros de la tabla.
                    }
                }
                else{ // Sino...
                    $resultset = null; // Almacena null
                }
            }
            return $resultset; // Finalmente retornla el arreglo con los elementos.
        }
    }
?>