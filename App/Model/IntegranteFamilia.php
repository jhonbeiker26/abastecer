<?php
    class IntegranteFamilia extends EntidadBase{
        // Atributos
        private $idIntegrante;
        private $cedulaIntegrante;
        private $nombreIntegrante;
        private $apellidoIntegrante;
        private $sexoIntegrante;
        private $fechaNacimiento;
        private $telefonoIntegrante;
        private $emailIntegrante;
        private $rolPersona;
        private $codigoCarnetPatria;
        private $serialCarnetPatria;
        private $manzanero;
        private $idFamilia;
        private $table;

        // Métodos
        public function __construct(){
            $this->table = "miembrofamilia";
            parent::__construct();
        }

        // Getter
        public function getIdIntegrante(){
            return $this->idIntegrante;
        }

        public function getCedulaIntegrante(){
            return $this->cedulaIntegrante;
        }

        public function getNombreIntegrante(){
            return $this->nombreIntegrante;
        }

        public function getApellidoIntegrante(){
            return $this->apellidoIntegrante;
        }

        public function getSexoIntegrante(){
            return $this->sexoIntegrante;
        }

        public function getFechaNacimiento(){
            return $this->fechaNacimiento;
        }

        public function getTelefonoIntegrante(){
            return $this->telefonoIntegrante;
        }

        public function getEmailIntegrante(){
            return $this->emailIntegrante;
        }

        public function getRolPersona(){
            return $this->rolPersona;
        }

        public function getCodigoCarnetPatria(){
            return $this->codigoCarnetPatria;
        }

        public function getSerialCarnetPatria(){
            return $this->serialCarnetPatria;
        }

        public function getManzanero(){
            return $this->manzanero;
        }

        public function getIdFamilia(){
            return $this->idFamilia;
        }

        // Setter
        public function setIdIntegrante($idIntegrante){
            $this->idIntegrante = $idIntegrante;
        }

        public function setCedulaIntegrante($cedulaIntegrante){
            $this->cedulaIntegrante = $cedulaIntegrante;
        }

        public function setNombreIntegrante($nombreIntegrante){
            $this->nombreIntegrante = $nombreIntegrante;
        }

        public function setApellidoIntegrante($apellidoIntegrante){
            $this->apellidoIntegrante = $apellidoIntegrante;
        }

        public function setSexoIntegrante($sexoIntegrante){
            $this->sexoIntegrante = $sexoIntegrante;
        }

        public function setFechaNacimiento($fechaNacimiento){
            $this->fechaNacimiento = $fechaNacimiento;
        }

        public function setTelefonoIntegrante($telefonoIntegrante){
            $this->telefonoIntegrante = $telefonoIntegrante;
        }

        public function setEmailIntegrante($emailIntegrante){
            $this->emailIntegrante = $emailIntegrante;
        }

        public function setRolPersona($rolPersona){
            $this->rolPersona = $rolPersona;
        }

        public function setCodigoCarnetPatria($codigoCarnetPatria){
            $this->codigoCarnetPatria = $codigoCarnetPatria;
        }

        public function setSerialCarnetPatria($serialCarnetPatria){
            $this->serialCarnetPatria = $serialCarnetPatria;
        }

        public function setManzanero($manzanero){
            $this->manzanero = $manzanero;
        }

        public function setIdFamilia($idFamilia){
            $this->idFamilia = $idFamilia;
        }

        public function getIntegranteByCedula(){
            $sql = "SELECT  miembrofamilia.idIntegrante,
                            miembrofamilia.nombreIntegrante, 
                            miembrofamilia.apellidoIntegrante, 
                            miembrofamilia.cedulaIntegrante,
                            miembrofamilia.telefonoIntegrante  
                            FROM miembrofamilia 
                            INNER JOIN grupofamiliar ON 
                            miembrofamilia.idFamilia = grupofamiliar.idFamilia
                            INNER JOIN clap ON 
                            grupofamiliar.idCLAP = clap.idClap WHERE 
                            clap.idClap = (SELECT grupoFamiliar.idCLAP FROM grupofamiliar
                            INNER JOIN  miembrofamilia ON 
                            grupofamiliar.idFamilia = miembrofamilia.idFamilia WHERE 
                            miembrofamilia.cedulaIntegrante = '$this->cedulaIntegrante') AND 
                            miembrofamilia.cedulaIntegrante = '$this->cedulaIntegrante'
                    ";
            $query = $this->DB()->query($sql);
            if($query){ // Evalua la cansulta
                if($query->rowCount() != 0) { // Si existe al menos un registro...
                    while($row = $query->fetch(PDO::FETCH_OBJ)) { // Recorre un array (tabla) fila por fila.
                        $resultset[] = $row; // Llena el array con cada uno de los registros de la tabla.
                    }
                }
                else{ // Sino...
                    $resultset[] = null; // Almacena null
                }
            }
            return $resultset; // Finalmente retornla el arreglo con los elementos.
        }
    }
?>