<?php
    class Atencion extends EntidadBase{
        private $fecha;
        private $observacion;
        private $tipoAtencion;
        private $cedula;
        private $clap;
        private $idFecha;
        private $table;

        public function __construct(){
            parent::__construct();
            $this->table = "atencion";
        }

        public function getIdfecha(){
            return $this->idFecha;
        }

        public function getCedula(){
            return $this->cedula;
        }

        public function getTipoAtencion(){
            return $this->tipoAtencion;
        }

        public function getObservacion(){
            return $this->observacion;
        }

        public function getFecha(){
            return $this->fecha;
        }

        public function getClap(){
            return $this->clap;
        }

        public function setIdFecha($idFecha){
            $this->idFecha=$idFecha;
        }

        public function setFecha($fecha){
            $this->fecha = $fecha;
        }

        public function setObservacion($observacion){
            $this->observacion = $observacion;
        }
        public function setTipoAtencion($tipoAtencion){
            $this->tipoAtencion = $tipoAtencion;
        }

        public function setCedula($cedula){
            $this->cedula = $cedula;
        }
        public function setClap($clap){
            $this->clap = $clap;
        }

        public function countFilasAll(){
            //Retorna el numero de filas que tiene la tabla
            $sql = "SELECT DISTINCT atencion.idAtencion,DATE_FORMAT(atencion.fechaAtencion,'%d/%m/%Y') AS fechaAtencion, 
                    clap.nombreClap , clap.parroquia FROM atencion 
                        INNER JOIN atencion_familia ON atencion_familia.idAtencion = atencion.idAtencion 
                        INNER JOIN grupofamiliar ON atencion_familia.idFamilia = grupofamiliar.idFamilia 
                        INNER JOIN clap ON grupofamiliar.idCLAP = clap.idClap";
            $result = $this->DB()->query($sql); // Ejecuta la consulta directamente.
            /*En caso tal de que la consultas ocurra un error retorno 0 para que mi controlador lo iguale  a null*/
            if($result){
                $numFila = $result->rowCount();
            }
            else{
                $numFila = 0;
            }
            return $numFila;
        }

        public function countFilasByFecha($desde,$hasta){
            $sql = "SELECT DISTINCT atencion.idAtencion,DATE_FORMAT(atencion.fechaAtencion,'%d/%m/%Y') AS fechaAtencion, 
                    clap.nombreClap,clap.parroquia FROM atencion 
                        INNER JOIN atencion_familia ON atencion_familia.idAtencion = atencion.idAtencion 
                        INNER JOIN grupofamiliar ON atencion_familia.idFamilia = grupofamiliar.idFamilia 
                        INNER JOIN clap ON grupofamiliar.idCLAP = clap.idClap 
                        WHERE atencion.fechaAtencion BETWEEN :desde AND :hasta"; // Consulta SQL
            $result = $this->DB()->prepare($sql); // Prepara la consulta SQL
            $result->bindParam(":desde",$desde);
            $result->bindParam(":hasta",$hasta);
            $result->execute(); // Ejecuta la consulta preparada.
            if($result){ // Si se ejecutó la consulta
                $numFilas = $result->rowCount(); // Cuenta la cantidad de filas afectadas
            }
            else{
                $numFilas = 0; // Sino, las filas = 0
            }
            return $numFilas;
        }

        public function getAll($numRegistros,$inicioLimit){
            $sql = "SELECT DISTINCT atencion.idAtencion,DATE_FORMAT(atencion.fechaAtencion,'%d/%m/%Y') AS fechaAtencion, 
                    clap.nombreClap,clap.parroquia FROM atencion 
                        INNER JOIN atencion_familia ON atencion_familia.idAtencion = atencion.idAtencion 
                        INNER JOIN grupofamiliar ON atencion_familia.idFamilia = grupofamiliar.idFamilia 
                        INNER JOIN clap ON grupofamiliar.idCLAP=clap.idClap ORDER BY atencion.idAtencion 
                        DESC LIMIT $inicioLimit,$numRegistros ";
            $resulLimit = $this->DB()->query($sql); // Ejecuta directamente la consulta SQL
            while($row = $resulLimit->fetch(PDO::FETCH_OBJ)){ // Recorre todos los registros como objetos
                $resulSet[] = $row; // Llena un array con objetos de la tabla
            }
            return $resulSet;
        }

        public function getAllByFechaAtencion($desde,$hasta,$numRegistros,$inicioLimit){
            $sql = "SELECT DISTINCT atencion.idAtencion,DATE_FORMAT(atencion.fechaAtencion,'%d/%m/%Y') AS fechaAtencion, 
                    clap.nombreClap,clap.parroquia FROM atencion 
                        INNER JOIN atencion_familia ON atencion_familia.idAtencion = atencion.idAtencion 
                        INNER JOIN grupofamiliar ON atencion_familia.idFamilia = grupofamiliar.idFamilia 
                        INNER JOIN clap ON grupofamiliar.idCLAP = clap.idClap 
                        WHERE atencion.fechaAtencion BETWEEN :desde AND :hasta LIMIT $inicioLimit,$numRegistros";
            $resultado=$this->DB()->prepare($sql); // Prepara la consulta SQL
            $resultado->bindParam(":desde",$desde);
            $resultado->bindParam(":hasta",$hasta);
            $resultado->execute(); // Ejecuta la consulta preparada
            while($row = $resultado->fetch(PDO::FETCH_OBJ)){
                    $resulSet[] = $row;
            }
            return $resulSet;
        }

        public function insert(){
            $sql = "INSERT INTO $this->table(fechaAtencion,observacion,tipoAtencion)
                    VALUES(:fecha,:observacion,:tipoAtencion)";
            $resultado = $this->DB()->prepare($sql); // Prepara la consulta
            $save = $resultado->execute(array(":fecha"=>$this->fecha,":observacion"=>$this->observacion,":tipoAtencion"=>$this->tipoAtencion));
            if($save){
                $idFecha = $this->getIdFechaNow();
            }
            return $idFecha;
        }

        public function getAtencionById($idAtencion){
            $sql = "SELECT  DATE_FORMAT(atencion.fechaAtencion,'%d/%m/%Y') AS fechaAtencion,
                    atencion.tipoAtencion,atencion.observacion,clap.nombreClap,clap.idClap,clap.parroquia, 
                    count(atencion_familia.idAtencion) AS cantidad FROM atencion 
                        INNER JOIN atencion_familia ON atencion.idAtencion = atencion_familia.idAtencion 
                        INNER JOIN grupofamiliar ON atencion_familia.idFamilia = grupofamiliar.idFamilia 
                        INNER JOIN clap ON grupofamiliar.idClap = clap.idClap 
                        WHERE atencion_familia.idAtencion = :id";
            $resultado = $this->DB()->prepare($sql); // Prepara la consulta SQL
            $resultado->bindParam(":id",$idAtencion);
            $resultado->execute(); // Ejecuta la consuluta preparada
            $row = $resultado->fetch(PDO::FETCH_OBJ); // Obtiene un registro de objetos.
            return $row;
        }

        public function insertPerson(){
            $sqlconsul = "SELECT grupofamiliar.idFamilia,grupofamiliar.apellidoFamilia FROM grupofamiliar 
                            INNER JOIN miembrofamilia ON grupofamiliar.idFamilia = miembrofamilia.idFamilia 
                            WHERE miembrofamilia.cedulaIntegrante = :cedula AND grupofamiliar.idCLAP = :idClap";
            $resultado = $this->DB()->prepare($sqlconsul); // Preprara la consulta SQL
            $resultado->bindParam(":cedula",$this->cedula);
            $resultado->bindParam(":idClap",$this->clap);
            $resultado->execute(); // Ejecuta la consulta SQL
            if($resultado->rowCount() >= 1){ // Si la cantidad de filas afectadas >= 1
                $row = $resultado->fetch(PDO::FETCH_OBJ); // Genera un registro de objetos
                $sqlInsert = "INSERT INTO atencion_familia (idAtencion,idFamilia)
                                VALUES($this->idFecha,$row->idFamilia)";
                $save=$this->DB()->query($sqlInsert); // Ejecuta directamente la consulta SQL
            }
            else{
                $row=$this->findCLAP();
            }
            return $row;
        }

         public function getIdFechaNow(){ //Devuelve la ultima fecha registrada por el sistema
            $sql = "SELECT idAtencion FROM $this->table ORDER by idAtencion DESC LIMIT 1";
            $resultado = $this->DB()->query($sql);
            $row = $resultado->fetch(PDO::FETCH_OBJ);
            return $row;
        }

        public function findCLAP(){
            $sql = "SELECT clap.nombreClap FROM clap WHERE idClap = 
                        (SELECT grupoFamiliar.idCLAP FROM grupofamiliar 
                            INNER JOIN miembrofamilia ON grupofamiliar.idFamilia = miembrofamilia.idFamilia 
                            WHERE miembrofamilia.cedulaIntegrante = $this->cedula)";
            $resul = $this->DB()->query($sql);
            if($resul->rowCount() >=1){
                $row = $resul->fetch(PDO::FETCH_OBJ);
            }
            else{
                $row = null;
            }
            return $row;
        }

        public function comprobarAtencion(){
            $band = false;
            $sql = "SELECT atencion.fechaAtencion FROM atencion 
                        INNER JOIN atencion_familia ON atencion_familia.idAtencion=atencion.idAtencion WHERE atencion_familia.idFamilia=(SELECt grupoFamiliar.idFamilia FROM grupofamiliar INNER JOIN miembrofamilia ON  grupofamiliar.idFamilia=miembrofamilia.idFamilia WHERE  miembrofamilia.cedulaIntegrante=$this->cedula)ORDER BY atencion.idAtencion DESC LIMIT 1";
            $result = $this->DB()->query($sql);
            $row = $result->fetch(PDO::FETCH_ASSOC);
            //Tomo la fecha de la base de datos y le sumo 30 dias
            $nuevaFecha = strtotime(('+30 day'),strtotime($row["fechaAtencion"]));
            $fecha = date("Y/m/d",$nuevaFecha);
            if($this->fecha >= $fecha && $this->fecha != $row["fechaAtencion"]){
                $band = true;
            }
            else{
                $band = false;
            }
            return $band;
        }

        public function comprobarGrupoFamiliar(){
            $band = false;
            $sql = "SELECT idAtencionFamilia FROM atencion_familia 
                    WHERE idFamilia = 
                        (SELECt grupoFamiliar.idFamilia FROM grupofamiliar 
                            INNER JOIN miembrofamilia ON grupofamiliar.idFamilia = miembrofamilia.idFamilia 
                            WHERE miembrofamilia.cedulaIntegrante = $this->cedula)";
            $result = $this->DB()->query($sql); 
            if($result->rowCount() >= 1){
                $band = true;
            }
            else{
                $band = false;
            }
            return $band;
        }

        public function  notification(){
            $band = true;
            $fechaActual = date("Y/m/d");
            $sql = "SELECT atencion.idAtencion,MAX(atencion.fechaAtencion) AS fechaAtencion,
                    clap.nombreClap FROM atencion 
                        INNER JOIN atencion_familia ON atencion_familia.idAtencion = atencion.idAtencion 
                        INNER JOIN grupofamiliar ON grupofamiliar.idFamilia = atencion_familia.idFamilia 
                        INNER JOIN clap ON clap.idClap = grupofamiliar.idCLAP GROUP BY clap.nombreClap";
            $result = $this->DB()->query($sql);
            if($result){
                if($result->rowCount() >= 1){
                    while($row = $result->fetch(PDO::FETCH_OBJ)){
                        $nuevaFecha=strtotime(('+30 day'),strtotime($row->fechaAtencion));
                        $fecha = date("Y/m/d",$nuevaFecha);
                        if($fechaActual >= $fecha){
                            $resultSet[] = $row;
                            $band = false;
                        }
                    }
                }
                else{
                    $resultSet = null;
                }
            }
            if($band){
                $resultSet = null;
            }
            return $resultSet;
        }
    }
?>